TabletResponsive={"1009":{
"pageLayer":{"w":1009,"h":724,"bgColor":"#ffffff","bgImage":"images/bkgnd004.gif","bgSize":"12px 1540px","bgRepeat":"repeat-x"}
,
"button248":{"B64":{"disabledState":"images/std_qt_cancel_normal.png","downState":"images/std_qt_cancel_clicked.png","normalState":"images/std_qt_cancel_normal.png","overState":"images/std_qt_cancel_over.png","selectedState":"images/std_qt_cancel_normal.png","visitedState":"images/std_qt_cancel_normal.png"},"bOffBottom":1,"h":41.0,"p":"M 0.000000 0.000000 L 60.000000 0.000000 L 60.000000 40.000000 L 0.000000 40.000000 L 0.000000 0.000000 z","stylemods":[{"decl":" { position:absolute; top:0.000000px; left:0.000000px; display:flex; justify-content:center; align-items:center; width:60.000000px; height:40.000000px;}","sel":"div.button248Text"},{"decl":" { width:56.000000px; margin-right:5px; margin-left:5px; text-align:center; line-height:16px; font-size:16px; font-family:\"Lucida Sans Unicode\"; color:#ffffff;}","sel":"span.button248Text"},{"decl":" { position:absolute; top:0.000000px; left:0.000000px; display:flex; justify-content:center; align-items:center; width:60.000000px; height:40.000000px;}","sel":"div.button248overStateText"},{"decl":" { width:56.000000px; margin-right:5px; margin-left:5px; text-align:center; line-height:16px; font-size:16px; font-family:\"Lucida Sans Unicode\"; color:#ffffff;}","sel":"span.button248overStateText"},{"decl":" { position:absolute; top:0.000000px; left:0.000000px; display:flex; justify-content:center; align-items:center; width:60.000000px; height:40.000000px;}","sel":"div.button248downStateText"},{"decl":" { width:56.000000px; margin-right:5px; margin-left:5px; text-align:center; line-height:16px; font-size:16px; font-family:\"Lucida Sans Unicode\"; color:#ffffff;}","sel":"span.button248downStateText"},{"decl":" { position:absolute; top:0.000000px; left:0.000000px; display:flex; justify-content:center; align-items:center; width:60.000000px; height:40.000000px;}","sel":"div.button248disabledStateText"},{"decl":" { width:56.000000px; margin-right:5px; margin-left:5px; text-align:center; line-height:16px; font-size:16px; font-family:\"Lucida Sans Unicode\"; color:#ffffff;}","sel":"span.button248disabledStateText"},{"decl":" { position:absolute; top:0.000000px; left:0.000000px; display:flex; justify-content:center; align-items:center; width:60.000000px; height:40.000000px;}","sel":"div.button248visitedStateText"},{"decl":" { width:56.000000px; margin-right:5px; margin-left:5px; text-align:center; line-height:16px; font-size:16px; font-family:\"Lucida Sans Unicode\"; color:#ffffff;}","sel":"span.button248visitedStateText"},{"decl":" { position:absolute; top:0.000000px; left:0.000000px; display:flex; justify-content:center; align-items:center; width:60.000000px; height:40.000000px;}","sel":"div.button248selectedStateText"},{"decl":" { width:56.000000px; margin-right:5px; margin-left:5px; text-align:center; line-height:16px; font-size:16px; font-family:\"Lucida Sans Unicode\"; color:#ffffff;}","sel":"span.button248selectedStateText"}],"w":61.0,"x":40,"y":664}
,
"button264":{"B64":{"disabledState":"images/std_qt_next_normal.png","downState":"images/std_qt_next_clicked.png","normalState":"images/std_qt_next_normal.png","overState":"images/std_qt_next_over.png","selectedState":"images/std_qt_next_normal.png","visitedState":"images/std_qt_next_normal.png"},"bOffBottom":1,"h":41.0,"p":"M 0.000000 0.000000 L 60.000000 0.000000 L 60.000000 40.000000 L 0.000000 40.000000 L 0.000000 0.000000 z","stylemods":[{"decl":" { position:absolute; top:0.000000px; left:0.000000px; display:flex; justify-content:center; align-items:center; width:60.000000px; height:40.000000px;}","sel":"div.button264Text"},{"decl":" { width:56.000000px; margin-right:5px; margin-left:5px; text-align:center; line-height:16px; font-size:16px; font-family:\"Lucida Sans Unicode\"; color:#ffffff;}","sel":"span.button264Text"},{"decl":" { position:absolute; top:0.000000px; left:0.000000px; display:flex; justify-content:center; align-items:center; width:60.000000px; height:40.000000px;}","sel":"div.button264overStateText"},{"decl":" { width:56.000000px; margin-right:5px; margin-left:5px; text-align:center; line-height:16px; font-size:16px; font-family:\"Lucida Sans Unicode\"; color:#ffffff;}","sel":"span.button264overStateText"},{"decl":" { position:absolute; top:0.000000px; left:0.000000px; display:flex; justify-content:center; align-items:center; width:60.000000px; height:40.000000px;}","sel":"div.button264downStateText"},{"decl":" { width:56.000000px; margin-right:5px; margin-left:5px; text-align:center; line-height:16px; font-size:16px; font-family:\"Lucida Sans Unicode\"; color:#ffffff;}","sel":"span.button264downStateText"},{"decl":" { position:absolute; top:0.000000px; left:0.000000px; display:flex; justify-content:center; align-items:center; width:60.000000px; height:40.000000px;}","sel":"div.button264disabledStateText"},{"decl":" { width:56.000000px; margin-right:5px; margin-left:5px; text-align:center; line-height:16px; font-size:16px; font-family:\"Lucida Sans Unicode\"; color:#ffffff;}","sel":"span.button264disabledStateText"},{"decl":" { position:absolute; top:0.000000px; left:0.000000px; display:flex; justify-content:center; align-items:center; width:60.000000px; height:40.000000px;}","sel":"div.button264visitedStateText"},{"decl":" { width:56.000000px; margin-right:5px; margin-left:5px; text-align:center; line-height:16px; font-size:16px; font-family:\"Lucida Sans Unicode\"; color:#ffffff;}","sel":"span.button264visitedStateText"},{"decl":" { position:absolute; top:0.000000px; left:0.000000px; display:flex; justify-content:center; align-items:center; width:60.000000px; height:40.000000px;}","sel":"div.button264selectedStateText"},{"decl":" { width:56.000000px; margin-right:5px; margin-left:5px; text-align:center; line-height:16px; font-size:16px; font-family:\"Lucida Sans Unicode\"; color:#ffffff;}","sel":"span.button264selectedStateText"}],"w":61.0,"x":909,"y":664}
,
"text287":{"x":6,"y":0,"w":994,"h":346,"txtscale":100,"bOffBottom":0}
,
"text288":{"x":82,"y":374,"w":816,"h":55,"txtscale":100,"bOffBottom":0}
,
"check289":{"x":52,"y":373,"fsize":16,"bOffBottom":0}
,
"text290":{"x":83,"y":425,"w":702,"h":37,"txtscale":100,"bOffBottom":0}
,
"check291":{"x":51,"y":429,"fsize":16,"bOffBottom":0}
,
"text292":{"x":82,"y":464,"w":823,"h":74,"txtscale":100,"bOffBottom":0}
,
"check293":{"x":52,"y":473,"fsize":16,"bOffBottom":0}
,
"text294":{"x":82,"y":546,"w":828,"h":55,"txtscale":100,"bOffBottom":0}
,
"check295":{"x":52,"y":545,"fsize":16,"bOffBottom":0}
,
"text296":{"x":82,"y":607,"w":767,"h":55,"txtscale":100,"bOffBottom":0}
,
"check297":{"x":52,"y":607,"fsize":16,"bOffBottom":0}
,
"text298":{"x":80,"y":668,"w":784,"h":56,"txtscale":100,"bOffBottom":0}
,
"check299":{"x":52,"y":666,"fsize":16,"bOffBottom":0}
,
"RCDResetQuestion":function(){
try{if(window.dragMgr)window.dragMgr.clearDropZones();}catch(e){if(e&&e.message)console.log(e.message);}
}
,
"RCDResultResize":function(){}
,"preload":[]
},
"785":{
"pageLayer":{"w":785,"h":1000,"bgColor":"#ffffff","bgImage":"images/bkgnd004.gif","bgSize":"9px 1198px","bgRepeat":"repeat-x"}
,
"button248":{"B64":{"disabledState":"images/std_qt_cancel_normal.png","downState":"images/std_qt_cancel_clicked.png","normalState":"images/std_qt_cancel_normal.png","overState":"images/std_qt_cancel_over.png","selectedState":"images/std_qt_cancel_normal.png","visitedState":"images/std_qt_cancel_normal.png"},"bOffBottom":1,"h":32.0,"p":"M 0.000000 0.000000 L 47.000000 0.000000 L 47.000000 31.000000 L 0.000000 31.000000 L 0.000000 0.000000 z","stylemods":[{"decl":" { position:absolute; top:0.000000px; left:0.000000px; display:flex; justify-content:center; align-items:center; width:47.000000px; height:31.000000px;}","sel":"div.button248Text"},{"decl":" { width:43.000000px; margin-right:5px; margin-left:5px; text-align:center; line-height:11px; font-size:11px; font-family:\"Lucida Sans Unicode\"; color:#ffffff;}","sel":"span.button248Text"},{"decl":" { position:absolute; top:0.000000px; left:0.000000px; display:flex; justify-content:center; align-items:center; width:47.000000px; height:31.000000px;}","sel":"div.button248overStateText"},{"decl":" { width:43.000000px; margin-right:5px; margin-left:5px; text-align:center; line-height:11px; font-size:11px; font-family:\"Lucida Sans Unicode\"; color:#ffffff;}","sel":"span.button248overStateText"},{"decl":" { position:absolute; top:0.000000px; left:0.000000px; display:flex; justify-content:center; align-items:center; width:47.000000px; height:31.000000px;}","sel":"div.button248downStateText"},{"decl":" { width:43.000000px; margin-right:5px; margin-left:5px; text-align:center; line-height:11px; font-size:11px; font-family:\"Lucida Sans Unicode\"; color:#ffffff;}","sel":"span.button248downStateText"},{"decl":" { position:absolute; top:0.000000px; left:0.000000px; display:flex; justify-content:center; align-items:center; width:47.000000px; height:31.000000px;}","sel":"div.button248disabledStateText"},{"decl":" { width:43.000000px; margin-right:5px; margin-left:5px; text-align:center; line-height:11px; font-size:11px; font-family:\"Lucida Sans Unicode\"; color:#ffffff;}","sel":"span.button248disabledStateText"},{"decl":" { position:absolute; top:0.000000px; left:0.000000px; display:flex; justify-content:center; align-items:center; width:47.000000px; height:31.000000px;}","sel":"div.button248visitedStateText"},{"decl":" { width:43.000000px; margin-right:5px; margin-left:5px; text-align:center; line-height:11px; font-size:11px; font-family:\"Lucida Sans Unicode\"; color:#ffffff;}","sel":"span.button248visitedStateText"},{"decl":" { position:absolute; top:0.000000px; left:0.000000px; display:flex; justify-content:center; align-items:center; width:47.000000px; height:31.000000px;}","sel":"div.button248selectedStateText"},{"decl":" { width:43.000000px; margin-right:5px; margin-left:5px; text-align:center; line-height:11px; font-size:11px; font-family:\"Lucida Sans Unicode\"; color:#ffffff;}","sel":"span.button248selectedStateText"}],"w":48.0,"x":31,"y":953}
,
"button264":{"B64":{"disabledState":"images/std_qt_next_normal.png","downState":"images/std_qt_next_clicked.png","normalState":"images/std_qt_next_normal.png","overState":"images/std_qt_next_over.png","selectedState":"images/std_qt_next_normal.png","visitedState":"images/std_qt_next_normal.png"},"bOffBottom":1,"h":32.0,"p":"M 0.000000 0.000000 L 47.000000 0.000000 L 47.000000 31.000000 L 0.000000 31.000000 L 0.000000 0.000000 z","stylemods":[{"decl":" { position:absolute; top:0.000000px; left:0.000000px; display:flex; justify-content:center; align-items:center; width:47.000000px; height:31.000000px;}","sel":"div.button264Text"},{"decl":" { width:43.000000px; margin-right:5px; margin-left:5px; text-align:center; line-height:11px; font-size:11px; font-family:\"Lucida Sans Unicode\"; color:#ffffff;}","sel":"span.button264Text"},{"decl":" { position:absolute; top:0.000000px; left:0.000000px; display:flex; justify-content:center; align-items:center; width:47.000000px; height:31.000000px;}","sel":"div.button264overStateText"},{"decl":" { width:43.000000px; margin-right:5px; margin-left:5px; text-align:center; line-height:11px; font-size:11px; font-family:\"Lucida Sans Unicode\"; color:#ffffff;}","sel":"span.button264overStateText"},{"decl":" { position:absolute; top:0.000000px; left:0.000000px; display:flex; justify-content:center; align-items:center; width:47.000000px; height:31.000000px;}","sel":"div.button264downStateText"},{"decl":" { width:43.000000px; margin-right:5px; margin-left:5px; text-align:center; line-height:11px; font-size:11px; font-family:\"Lucida Sans Unicode\"; color:#ffffff;}","sel":"span.button264downStateText"},{"decl":" { position:absolute; top:0.000000px; left:0.000000px; display:flex; justify-content:center; align-items:center; width:47.000000px; height:31.000000px;}","sel":"div.button264disabledStateText"},{"decl":" { width:43.000000px; margin-right:5px; margin-left:5px; text-align:center; line-height:11px; font-size:11px; font-family:\"Lucida Sans Unicode\"; color:#ffffff;}","sel":"span.button264disabledStateText"},{"decl":" { position:absolute; top:0.000000px; left:0.000000px; display:flex; justify-content:center; align-items:center; width:47.000000px; height:31.000000px;}","sel":"div.button264visitedStateText"},{"decl":" { width:43.000000px; margin-right:5px; margin-left:5px; text-align:center; line-height:11px; font-size:11px; font-family:\"Lucida Sans Unicode\"; color:#ffffff;}","sel":"span.button264visitedStateText"},{"decl":" { position:absolute; top:0.000000px; left:0.000000px; display:flex; justify-content:center; align-items:center; width:47.000000px; height:31.000000px;}","sel":"div.button264selectedStateText"},{"decl":" { width:43.000000px; margin-right:5px; margin-left:5px; text-align:center; line-height:11px; font-size:11px; font-family:\"Lucida Sans Unicode\"; color:#ffffff;}","sel":"span.button264selectedStateText"}],"w":48.0,"x":707,"y":953}
,
"text287":{"x":5,"y":0,"w":773,"h":437,"txtscale":100,"bOffBottom":0}
,
"text288":{"x":64,"y":374,"w":635,"h":54,"txtscale":100,"bOffBottom":0}
,
"check289":{"x":40,"y":373,"fsize":16,"bOffBottom":0}
,
"text290":{"x":65,"y":425,"w":546,"h":54,"txtscale":100,"bOffBottom":0}
,
"check291":{"x":39,"y":429,"fsize":16,"bOffBottom":0}
,
"text292":{"x":64,"y":464,"w":640,"h":81,"txtscale":100,"bOffBottom":0}
,
"check293":{"x":40,"y":473,"fsize":16,"bOffBottom":0}
,
"text294":{"x":64,"y":546,"w":644,"h":54,"txtscale":100,"bOffBottom":0}
,
"check295":{"x":40,"y":545,"fsize":16,"bOffBottom":0}
,
"text296":{"x":64,"y":607,"w":597,"h":54,"txtscale":100,"bOffBottom":0}
,
"check297":{"x":40,"y":607,"fsize":16,"bOffBottom":0}
,
"text298":{"x":62,"y":668,"w":610,"h":54,"txtscale":100,"bOffBottom":0}
,
"check299":{"x":40,"y":666,"fsize":16,"bOffBottom":0}
,
"RCDResetQuestion":function(){
try{if(window.dragMgr)window.dragMgr.clearDropZones();}catch(e){if(e&&e.message)console.log(e.message);}
}
,
"RCDResultResize":function(){}
,"preload":[]
}}
